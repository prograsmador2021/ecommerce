
let traerProductos = () => {
    const traerDelServidor = async () => {
        const resultado = await fetch('/productos/')
        let resultadoJson = await resultado.json()
        // .then(response => response.json())
        let template = await `
        <div class="slick-track" style="opacity: 1; width: 879px; transform: translate3d(0px, 0px, 0px);">
    ${resultadoJson.map(producto =>
            `
            <div class="product slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="0" style="width: 263px;">
        <div class="product">
            <div class="product-img">
                <img src="./img/product01.png" alt="">
                <div class="product-label">
                    <span class="sale">-30%</span>
                    <span class="new">NEW</span>
                </div>
            </div>
            <div class="product-body">
                <p class="product-category">Category</p>
                <h3 class="product-name"><a href="product.html">${producto.nombre}</a></h3>
                <h4 class="product-price">${producto.precio} <del class="product-old-price">${producto.precio}</del></h4>
                <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
                <div class="product-btns">
                    <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
                    <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
                    <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
                </div>
            </div>
            <div class="add-to-cart">
                <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
            </div>
        </div>
        </div>
        `
        ).join('')}
        </div>
        `
        document.querySelector("#contentProduct").innerHTML = template;

    }
    traerDelServidor()
}

// traerProductos()

let misProductos = () => {
    fetch('/productos', { method: 'GET' })
        .then(response => response.json())
        .then(listadoDeProductos => listadoDeProductos.map(
            porCadaProducto => {
                let template =
                    `
            <div class="product slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="0" style="width: 263px;">
        <div class="product">
            <div class="product-img">
                <img src="${porCadaProducto.photo}" alt="">
                <div class="product-label">
                    <span class="sale">-30%</span>
                    <span class="new">NEW</span>
                </div>
            </div>
            <div class="product-body">
                <p class="product-category">Category</p>
                <h3 class="product-name" data-idproduct="${porCadaProducto.id}"><a href="product.html">${porCadaProducto.nombre}</a></h3>
                <h4 class="product-price">$${porCadaProducto.precio} <del class="product-old-price">$${porCadaProducto.precio}</del></h4>
                <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
                <div class="product-btns">
                    <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
                    <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
                    <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
                </div>
            </div>
            <div class="add-to-cart">
                <button onclick="addToCart('${porCadaProducto.nombre}','${porCadaProducto.precio}','${porCadaProducto.id}')" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
            </div>
        </div>
        </div>
            `
                document.querySelector('#contentProduct').innerHTML += template
            }))
        .then()
}

let addToCart = (miProduct, miPrecio, miId) => {
    // console.log(`Agregamos ${miProduct} al carrito por ${miPrecio}`)
    console.log("mi ID", miId)
    let miProductoASubir = {};
    miProductoASubir.id = miId
    console.log(miProductoASubir)
    fetch('/cart',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
              },
            body: JSON.stringify(miProductoASubir)
        }

    ).then(cartUpdateView())
    .then(cartQuantity())

}

let cartQuantity = () => {
    console.log('Por actualizar la cantidad del carrito ')
    fetch('/cartQuantity', { method: 'GET' })
    .then(response => response.json())
    .then(response = document.querySelector('#cartQuantity').value= response)
    .then(response = console.log(response))
}

let cartUpdateView = () => {
    document.querySelector('#cart').innerHTML = ""
    console.log('por actualizar el carrito')
    fetch('/cart', { method: 'GET' })
    .then(response => response.json())
    .then(CartList => CartList.map(
        porCadaProducto => {
            console.log(porCadaProducto.nombre)
            let template =
                `<div class="product-widget">
                    <div class="product-img">
                        <img src="${porCadaProducto.photo}" alt="">
                    </div>
                    <div class="product-body">
                        <h3 class="product-name"><a href="#">${porCadaProducto.nombre}</a></h3>
                        <h4 class="product-price"><span class="qty">1x</span>${porCadaProducto.precio}</h4>
                    </div>
                    <button class="delete"><i class="fa fa-close"></i></button>
                </div>
                `
                document.querySelector('#cart').innerHTML += template
            })) 
}

misProductos()
cartQuantity()
cartUpdateView()
const { request, response } = require('express')
const path = require('path');
const express = require('express')
const app = express()
 
// let listado = [
//     {"id":1, "nombre": "Celular", "precio" : 30000, photo : 'https://pixelstore.com.ar/wp-content/uploads/2021/07/nokia-2...jpg'},
//     {"id":2, "nombre": "Notebook", "precio" : 100000, photo : 'https://www.nsx.com.ar/archivos/N_omicron_1.png'},
//     {"id":3, "nombre": "Mouse", "precio" : 1000, photo : 'http://snpi.dell.com/snp/images/products/large/dell_wm126_wireless_optical_mouse_blk_3_l.jpg'},
//      {"id":4, "nombre": "Teclado", "precio" : 2000, photo : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhWbQFLZdiYX4uABqjXrLM05W3BMOabWZdcQ&usqp=CAU'},
//     {"id":5, "nombre": "Cable hdmi", "precio" : 900, photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqWfcQ-bT1lfxq7D3V5K3nPuTh6ZHaZBetlZr8NRhLIaK8RRmzYxLvzigLA1mazGoRy5U&usqp=CAU'},
// ]


let listado = []
let mysql = require('mysql')
let connection = mysql.createConnection(
    {
        host:'localhost',
        user : 'educacion',
        password : 'educacion',
        database : 'ecommerce'
    }
)


connection.connect()

connection.query('SELECT * FROM `products`', (error, results) => {
    console.log(results)
    listado = results
})

app.use(express.json())


let cart = []

app.get('/', (request, response) => {
    response.send('<h1>Este es el Home</h1>')
})

app.get('/productos', (request, response) => {
    // response.send('<h1>Productos de oferta</h1>')
    response.json(listado)
})


app.get('/productos/:id', (request, response) => {
    const id = Number(request.params.id)
    const producto = listado.find( elemento => elemento.id === id)
    response.json(producto)
})

app.post('/cart', (request, response) => {
    const producto = request.body
    console.log(producto)
    const productoEncontrado = listado.find( item => item.id === Number(producto.id) )
    console.log('Producto encontrado',productoEncontrado)
    // const productToCart = {}
    // productToCart.id = Number(producto.id)
    // productToCart.cant = 1
    productoEncontrado.cant = 1 
    cart.push(productoEncontrado)
    response.json(productoEncontrado)
})

app.get('/cart', (request, response) => {
    response.json(cart)
})

app.get('/cartQuantity', (request, response) => {
    response.json(cart.length)
})

app.use(express.static('public'))
app.use('/public', express.static('public'))

const port = 3001
app.listen(port, () => {console.log(`Server is running on port ${port} `)})



// 1. Agregrar producto al carrito
//      a. Enviar por metodo POST el producto al array/DB
//      b. Actualizar la vista del carrito

// 2. En el carrito mostrar productos
//      a. Tenemos que consultar por metodo GET y pedir el carrito
//      b. Dibujar en pantalla con HTML al carrito